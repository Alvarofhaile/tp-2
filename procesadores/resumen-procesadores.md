# Modelo de Von Neumann

## Resumen

Este tipo de arquitectura de computadora fue propuesto por el físico y matemático John Von Neumann en el año 1945. A pesar de ser un modelo antiguo sigue siendo la base estructural con la que se fabrican la mayor parte de las computadoras actuales.
Un diagrama básico de esta arquitectura puede ser representada mediante la  figura 1:

![](https://www.lifeder.com/wp-content/uploads/2019/10/Arquitectura-von-Neumann-De-David-strigoi-Trabajo-propio-Dominio-p%C3%BAblico-httpscommons.wikimedia.orgwindex.phpcurid7924651.jpg)
(Figura 1: Diagrama de la Arquitectura Von Neumann)

Este modelo consta de 3 partes principales que se describirán a continuación: La memoria, el CPU(Central Processing Unit) y los periféricos de salida/entrada.

---
## Memoria principal

En este componente del sistema se almacenan de forma volátil palabras de información, que son cantidades mínimas de bits de información a las que el CPU hace y/o hara uso. Esta información contenida puede ser programas que se están ejecutando actualmente o datos obtenidos de memorias secundarias.

## Sistemas de entrada-salida (I/0)

Son aquellos dispositivos, ajenos al CPU, que se vinculan mediante el bus de datos a esta. Cada uno de estos dispositivos poseen un procesador dedicado exclusivamente a regular el acceso al bus de datos.

Los dispositivos de entrada (I) son aquellos que envian información a la CPU, ejemplos: Teclados, mouses, etc.

Los dispositivos de salida (O) son aquellos que reciben información del CPU, ejemplos: Monitores, impresoras, etc.

## CPU
Es la unidad encargada de la búsqueda, decodificación y ejecución de instrucciones almacenadas en la memoria principal.
Esta se compone de:

1.**Unidad de control:**
Es la responsable de buscar instrucciones y datos, cuando la CPU lo requiera, en la memoria principal de la computadora y además determinar el tipo de esta.

2.**Unidad aritmético-lógica(ALU):**
Se encarga de realizar las operaciones matemáticas y lógicas basicas del procesador.

3.**Registros internos:**
Estas conforman una memoria de pequeño espacio pero de rápido acceso para el CPU, que almacena información de forma volátil. Cada registro posee un tamaño y función única dedicados a optimizar el rendimiento del CPU. El registro más importante es el Contador de Programa (PC) que apunta a la siguiente instrucción que debe ejecutar el procesador. También está el Registro de Instrucción (IR) que mantiene almacenada la instrucción que se está ejecutando actualmente. Las computadoras generalmente poseen muchos mas registros.

## Bus de datos

Son una colección de conductores eléctricos que se encargan de ser el medio por el cual se transmiten direcciones, datos e instrucciones de una unidad a otra. Es fundamental que una computadora coordine la utilización del bus de datos, ya que sobre este actuan los dispositivos (I/O), la memoria principal y el CPU.
Es importante notar que el Bus puede ser interno o externo al CPU, ya que sus sub-unidades también se deben vincular entre si.

Fuentes bibliográficas: 

**Structured Computer Organization**, Andrew S. Tanenbaum, sexta edición, año 2013
