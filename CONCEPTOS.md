# Práctica 2: Markdown y herramientas de programación colaborativas
## Parte 1. Git y Markdown
### Git
**Definase los siguientes conceptos:**

1.**Software de control de versiones:**
Es aquel programa informático que se encarga de administrar las distintas versiones de un proyecto y coordinar el trabajo grupal realizado sobre los archivos compartidos.

2.**Repositorios:**
Es el espacio informático en el que se guardan todos los archivos que componen el proyecto, incluyendo sus versiones. Este puede ser público si el acceso no posee restricciones y privado si únicamente un grupo en particular tiene acceso.

3.**Arboles de Merkle:**
Es una estructura de datos en forma de árbol que nos permite relacionar una gran cantidad de datos en un único punto (nodo raíz o raíz Merkle). Inicialmente los nodos hojas son los iniciales que se asocian con un nodo padre, el cual posee un identificador único resultado de la asociación con sus hijos. Sucesivamente estos padres se asociaron con otros pares para formar nuevos nodos y así, la estructura de Merkle que sucumbe en un nodo raíz. Los arboles Merkle permiten verificar y validar una gran cantidad de datos solo analizando el nodo raíz. Ya que este cambiara si los datos asociados a los nodos hojas varían. Por otro lado sirve como mecanismo de seguridad ante manipulaciones, ya que al cambiar un nodo, el árbol completo se invalidara
El software de Git utiliza esta diagramación, permitiendo que se pueda hacer un seguimiento de cambios dentro del repositorio de datos. Ante un cambio en la raíz de Merkle, se genera un nuevo árbol y se preserva el anterior

4.**Copia local y remota de datos:**
Una copia local de datos consta de hacer un backup de datos en el dispositivo en el que se trabaja tal archivo o en otros dispositivos físicos. Por otro lado la copia remota de datos consta de utilizar servidores de internet para almacenar estas copias.

5.**¿Qué es un commit y cuando deberías hacerlo?:**
Un commit es el estado de un proyecto a cierto instante, debería ser establecido una vez que la versión actual sea revisada y aceptada por los participantes del trabajo en general. Esto se hace mediante el comando “pull request” que une una rama secundaria a la rama principal o “master”. Una vez revisado por los demás usuarios se le dara “merge” a esta rama y creara un nuevo commit.

6.**¿Cómo volverías a una versión anterior de tu trabajo si te das cuenta que gran parte de lo que avanzaste debe ser modificado?:**
Al acceder a versiones anteriores del trabajo, puedo volver a un punto en el que esas modificaciones no se encuentren aplicadas a mi trabajo. Para hacer esto accedo al nodo respectivo que me solucione este problema.

7.**¿Qué son los branches (ramas) y para que se usan?:**
Los branches de nuestro repositorio son espacios de trabajo sobre nuestro proyecto en los cuales podemos hacer modificaciones a nuestra branche principal o “master”, sin afectar a la versión estable de esta. Al verificar que nuestra rama secundaria es aceptable por todos los participantes del proyecto se le puede dar “merge” y crear una nueva versión del proyecto incluyendo esta modificación.

Fuentes:

https://medium.com/@janpoloy/qu%C3%A9-es-y-para-que-sirve-git-3fd106e6e137

https://academy.bit2me.com/que-es-un-arbol-merkle/

----
### Markdown

1.**¿Qué es el lenguaje Markdown?:**
El Markdown es un lenguaje que nos permite la aplicación de formatos a un texto específico mediante el empleo de ciertos caracteres que se encargan de formar el programa escrito en Markdown.

2.**¿Para que se lo utiliza y cuales son las ventajas de este lenguaje?:**
La principal aplicación es la elaboración de textos de diversas índoles, por otro lado la computadora interpreta a este lenguaje de forma automática como HTML. Esto puede implicar un ahorro de tiempo en la programación de un archivo HTML.
La sintaxis es sumamente sencilla y ofrece una rapidez alta a la hora de aplicar configuraciones a un texto. También es difícil cometer errores de sintaxis, nuestro entorno de trabajo es minimalista y es hasta práctico a la hora de utilizarlo en dispositivos móviles que no utilizan un teclado físico.

Fuentes:

https://www.genbeta.com/guia-de-inicio/que-es-markdown-para-que-sirve-y-como-usarlo